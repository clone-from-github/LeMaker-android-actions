#ifndef ACTIM_RESIZE
#define ACTIM_RESIZE

int downsample_image(unsigned char *pIn, int nInW, int nInH, unsigned char *pOut, int nOW, int nOH, int mode);


#endif
