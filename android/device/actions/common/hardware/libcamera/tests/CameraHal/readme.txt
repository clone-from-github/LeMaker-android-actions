A   Select Camera
[   Resume Preview after capture
0   Reset to defaults
q   Quit
@   Disconnect and Reconnect to CameraService
/   Enable/Disable showfps
a   GEO tagging settings menu
E   Camera Capability Dump


1   Start Preview
2   Stop Preview
~   Preview format
4   Preview size
&   Dump a preview frame

p   Take picture
$   Picture Format
3   Picture Rotation
5   Picture size
i   ISO mode
o   Jpeg Quality
:   Thumbnail Size
'   Thumbnail Quality

6   Start Video Recording
2   Stop Recording
l   Video Capture resolution
]   Video Bit rate
9   Video Codec
D   Audio Codec
v   Output Format
r   Framerate


F   Start face detection
T   Stop face detection
G   Touch/Focus area AF
f   Auto Focus/Half Press
J   Flash Modes
7   EV offset
8   AWB mode
z   Zoom 
e   Effect
w   Scene
s   Saturation
c   Contrast
h   Sharpness
b   Brightness
x   Antibanding
g   Focus mode
<   Exposure Lock
>   WhiteBalance Lock

