﻿<?xml version="1.0" encoding="utf-8" ?>
<!-- Copyright (C) 2012 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->

<!--
<!DOCTYPE MediaCodecs [
<!ELEMENT MediaCodecs (Decoders,Encoders)>
<!ELEMENT Decoders (MediaCodec*)>
<!ELEMENT Encoders (MediaCodec*)>
<!ELEMENT MediaCodec (Type*,Quirk*)>
<!ATTLIST MediaCodec name CDATA #REQUIRED>
<!ATTLIST MediaCodec type CDATA>
<!ELEMENT Type EMPTY>
<!ATTLIST Type name CDATA #REQUIRED>
<!ELEMENT Quirk EMPTY>
<!ATTLIST Quirk name CDATA #REQUIRED>
]>

There's a simple and a complex syntax to declare the availability of a
media codec:

A codec that properly follows the OpenMax spec and therefore doesn't have any
quirks and that only supports a single content type can be declared like so:

    <MediaCodec name="OMX.foo.bar" type="something/interesting" />

If a codec has quirks OR supports multiple content types, the following syntax
can be used:

    <MediaCodec name="OMX.foo.bar" >
        <Type name="something/interesting" />
        <Type name="something/else" />
        ...
        <Quirk name="requires-allocate-on-input-ports" />
        <Quirk name="requires-allocate-on-output-ports" />
        <Quirk name="output-buffers-are-unreadable" />
    </MediaCodec>

Only the three quirks included above are recognized at this point:

"requires-allocate-on-input-ports"
    must be advertised if the component does not properly support specification
    of input buffers using the OMX_UseBuffer(...) API but instead requires
    OMX_AllocateBuffer to be used.

"requires-allocate-on-output-ports"
    must be advertised if the component does not properly support specification
    of output buffers using the OMX_UseBuffer(...) API but instead requires
    OMX_AllocateBuffer to be used.

"output-buffers-are-unreadable"
    must be advertised if the emitted output buffers of a decoder component
    are not readable, i.e. use a custom format even though abusing one of
    the official OMX colorspace constants.
    Clients of such decoders will not be able to access the decoded data,
    naturally making the component much less useful. The only use for
    a component with this quirk is to render the output to the screen.
    Audio decoders MUST NOT advertise this quirk.
    Video decoders that advertise this quirk must be accompanied by a
    corresponding color space converter for thumbnail extraction,
    matching surfaceflinger support that can render the custom format to
    a texture and possibly other code, so just DON'T USE THIS QUIRK.

-->

<MediaCodecs>
    <Decoders>
        <MediaCodec name="OMX.Action.Video.Decoder" >
            <Type name="video/avc">
            	<Feature name="adaptive-playback" />
            </Type>
            <Type name="video/x-vnd.on2.vp8">
            	<Feature name="adaptive-playback" />
            </Type>    
            <Type name="video/mpeg2" />
            <Type name="video/div3" />
            <Type name="video/mp4v-es" />
            <Type name="video/3gpp" />
            <Type name="video/flv1" />
            <Type name="video/vc1" />
            <Type name="video/rv" />
            <Type name="video/mjpg" />
            <Type name="video/rvg2" />
            <Type name="video/wmv8" />
            <Type name="video/vp6" />
            <Type name="video/hevc_91" />
            <Type name="image/jpeg" />
            <Type name="video/avs" />
            <Type name="video/hevc_actions" />
            <Type name="video/vp9_actions" />
            <Quirk name="requires-allocate-on-input-ports" />
            <Quirk name="requires-allocate-on-output-ports" />
            <Quirk name="input-buffer-sizes-are-bogus" />
            <Quirk name="needs-flush-before-disable" />
        </MediaCodec>
        
        <MediaCodec name="OMX.Action.Video.Decoder.Deinterlace" >
            <Type name="video/mpeg2" />
            <Quirk name="requires-allocate-on-input-ports" />
            <Quirk name="requires-allocate-on-output-ports" />
            <Quirk name="input-buffer-sizes-are-bogus" />
            <Quirk name="needs-flush-before-disable" />
        </MediaCodec>
        
        <MediaCodec name="OMX.Action.Audio.Decoder" >
            <Type name="audio/mpeg-L1" />
            <Type name="audio/mpeg-L2" />
            <Type name="audio/mpeg" />
            <Type name="audio/vorbis" />
            <Type name="audio/3gpp" />
            <Type name="audio/amr-wb" />
            <Type name="audio/flac" />
            <Type name="audio/AAC" />
            <Type name="audio/WMASTD" />
            <Type name="audio/WMALSL" />
            <Type name="audio/WMAPRO" />            
            <Type name="audio/APE" />
            <Type name="audio/COOK" />
            <Type name="audio/DTS" />
            <Type name="audio/AC3" />
            <Type name="audio/PCM" />
            <Type name="audio/ADPCM" />
            <Type name="audio/ACELP" />
            <Type name="audio/AIFF" />
            <Type name="audio/MPC" />                         
            <Quirk name="requires-allocate-on-input-ports" />
            <Quirk name="requires-allocate-on-output-ports" />
        </MediaCodec>

         <MediaCodec name="OMX.google.mp3.decoder" type="audio/mpeg">
            <Limit name="channel-count" max="2" />
            <Limit name="sample-rate" ranges="8000,11025,12000,16000,22050,24000,32000,44100,48000" />
            <Limit name="bitrate" range="8000-320000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.amrnb.decoder" type="audio/3gpp">
            <Limit name="channel-count" max="1" />
            <Limit name="sample-rate" ranges="8000" />
            <Limit name="bitrate" range="4750-12200" />
        </MediaCodec>
        <MediaCodec name="OMX.google.amrwb.decoder" type="audio/amr-wb">
            <Limit name="channel-count" max="1" />
            <Limit name="sample-rate" ranges="16000" />
            <Limit name="bitrate" range="6600-23850" />
        </MediaCodec>
        <MediaCodec name="OMX.google.aac.decoder" type="audio/mp4a-latm">
            <Limit name="channel-count" max="8" />
            <Limit name="sample-rate" ranges="7350,8000,11025,12000,16000,22050,24000,32000,44100,48000" />
            <Limit name="bitrate" range="8000-960000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.g711.alaw.decoder" type="audio/g711-alaw">
            <Limit name="channel-count" max="1" />
            <Limit name="sample-rate" ranges="8000" />
            <Limit name="bitrate" range="64000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.g711.mlaw.decoder" type="audio/g711-mlaw">
            <Limit name="channel-count" max="1" />
            <Limit name="sample-rate" ranges="8000" />
            <Limit name="bitrate" range="64000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.vorbis.decoder" type="audio/vorbis">
            <Limit name="channel-count" max="8" />
            <Limit name="sample-rate" ranges="8000-96000" />
            <Limit name="bitrate" range="32000-500000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.opus.decoder" type="audio/opus">
            <Limit name="channel-count" max="8" />
            <Limit name="sample-rate" ranges="48000" />
            <Limit name="bitrate" range="6000-510000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.raw.decoder" type="audio/raw">
            <Limit name="channel-count" max="8" />
            <Limit name="sample-rate" ranges="8000-96000" />
            <Limit name="bitrate" range="1-10000000" />
        </MediaCodec>

        <MediaCodec name="OMX.google.mpeg4.decoder" type="video/mp4v-es">
            <!-- profiles and levels:  ProfileSimple : Level3 -->
            <Limit name="size" min="2x2" max="352x288" />
            <Limit name="alignment" value="2x2" />
            <Limit name="block-size" value="16x16" />
            <Limit name="blocks-per-second" range="12-11880" />
            <Limit name="bitrate" range="1-384000" />
            <Feature name="adaptive-playback" />
        </MediaCodec>
        <MediaCodec name="OMX.google.h263.decoder" type="video/3gpp">
            <!-- profiles and levels:  ProfileBaseline : Level30, ProfileBaseline : Level45
                    ProfileISWV2 : Level30, ProfileISWV2 : Level45 -->
            <Limit name="size" min="2x2" max="352x288" />
            <Limit name="alignment" value="2x2" />
            <Limit name="bitrate" range="1-384000" />
            <Feature name="adaptive-playback" />
        </MediaCodec>
        <MediaCodec name="OMX.google.h264.decoder" type="video/avc">
            <!-- profiles and levels:  ProfileBaseline : Level51 -->
            <Limit name="size" min="2x2" max="352x288" />
            <Limit name="alignment" value="2x2" />
            <Limit name="block-size" value="16x16" />
            <Limit name="blocks-per-second" range="1-983040" />
            <Limit name="bitrate" range="1-40000000" />
            <Feature name="adaptive-playback" />
        </MediaCodec>
        <MediaCodec name="OMX.google.hevc.decoder" type="video/hevc">
            <!-- profiles and levels:  ProfileMain : MainTierLevel51 -->
            <Limit name="size" min="2x2" max="1280x720" />
            <Limit name="alignment" value="2x2" />
            <Limit name="block-size" value="8x8" />
            <Limit name="block-count" range="1-139264" />
            <Limit name="blocks-per-second" range="1-2000000" />
            <Limit name="bitrate" range="1-10000000" />
            <Feature name="adaptive-playback" />
        </MediaCodec>
        <MediaCodec name="OMX.google.vp8.decoder" type="video/x-vnd.on2.vp8">
            <Limit name="size" min="2x2" max="1280x720" />
            <Limit name="alignment" value="2x2" />
            <Limit name="block-size" value="16x16" />
            <Limit name="blocks-per-second" range="1-1000000" />
            <Limit name="bitrate" range="1-40000000" />
            <Feature name="adaptive-playback" />
        </MediaCodec>
        <MediaCodec name="OMX.google.vp9.decoder" type="video/x-vnd.on2.vp9">
            <Limit name="size" min="2x2" max="1280x720" />
            <Limit name="alignment" value="2x2" />
            <Limit name="block-size" value="16x16" />
            <Limit name="blocks-per-second" range="1-500000" />
            <Limit name="bitrate" range="1-40000000" />
            <Feature name="adaptive-playback" />
        </MediaCodec>
    </Decoders>

    <Encoders>
        <MediaCodec name="OMX.Action.Video.Encoder" type="video/avc">
            <Limit name="size" min="176*144" max="2048x1536" />
            <Quirk name="requires-allocate-on-output-ports" />
            <Quirk name="requires-loaded-to-idle-after-allocation" />
            <Quirk name="defers-output-buffer-allocation" />
            <Quirk name="avoid-memcopy-input-recording-frames" />
            <Quirk name="act-muse-new-media-buffer" />
        </MediaCodec>
        
        <MediaCodec name="OMX.google.h263.encoder" type="video/3gpp">
            <!-- profiles and levels:  ProfileBaseline : Level45 -->
            <Limit name="size" min="176x144" max="176x144" />
            <Limit name="alignment" value="16x16" />
            <Limit name="bitrate" range="1-128000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.h264.encoder" type="video/avc">
            <!-- profiles and levels:  ProfileBaseline : Level2 -->
            <Limit name="size" min="64x64" max="896x896" />
            <Limit name="alignment" value="16x16" />
            <Limit name="block-size" value="16x16" />
            <Limit name="blocks-per-second" range="1-11880" />
            <Limit name="bitrate" range="1-2000000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.mpeg4.encoder" type="video/mp4v-es">
            <!-- profiles and levels:  ProfileCore : Level2 -->
            <Limit name="size" min="64x64" max="176x144" />
            <Limit name="alignment" value="16x16" />
            <Limit name="block-size" value="16x16" />
            <Limit name="blocks-per-second" range="12-1485" />
            <Limit name="bitrate" range="1-64000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.vp8.encoder" type="video/x-vnd.on2.vp8">
            <!-- profiles and levels:  ProfileMain : Level_Version0-3 -->
            <Limit name="size" min="64x64" max="1280x720" />
            <Limit name="alignment" value="2x2" />
            <Limit name="bitrate" range="1-40000000" />
            <Feature name="bitrate-modes" value="VBR,CBR" />
        </MediaCodec>
        <MediaCodec name="OMX.google.aac.encoder" type="audio/mp4a-latm">
            <Limit name="channel-count" max="6" />
            <Limit name="sample-rate" ranges="8000,11025,12000,16000,22050,24000,32000,44100,48000" />
            <Limit name="bitrate" range="8000-960000" />
        </MediaCodec>
        <MediaCodec name="OMX.google.amrnb.encoder" type="audio/3gpp">
            <Limit name="channel-count" max="1" />
            <Limit name="sample-rate" ranges="8000" />
            <Limit name="bitrate" range="4750-12200" />
            <Feature name="bitrate-modes" value="CBR" />
        </MediaCodec>
        <MediaCodec name="OMX.google.amrwb.encoder" type="audio/amr-wb">
            <Limit name="channel-count" max="1" />
            <Limit name="sample-rate" ranges="16000" />
            <Limit name="bitrate" range="6600-23850" />
            <Feature name="bitrate-modes" value="CBR" />
        </MediaCodec>
        <MediaCodec name="OMX.google.flac.encoder" type="audio/flac">
            <Limit name="channel-count" max="2" />
            <Limit name="sample-rate" ranges="1-655350" />
            <Limit name="bitrate" range="1-21000000" />
            <Limit name="complexity" range="0-8"  default="5" />
            <Feature name="bitrate-modes" value="CQ" />
        </MediaCodec>
    </Encoders>
</MediaCodecs>
