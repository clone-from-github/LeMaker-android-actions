/********************************************************************************
*                              5003
*                            Module: extension definition
*                 Copyright(c) 2003-2008 Actions Semiconductor,
*                            All Rights Reserved.
*
* History:
*      <author>    <time>           <version >             <desc>
*       kkli     2008-08-26 11:00     1.0             build this file
********************************************************************************/
/*!
* \file     extdef.h
* \brief    提供后缀名所对应的字符串
* \author   kkli
* \version 1.0
* \date  2008/08/26
*******************************************************************************/
#ifndef __EXTDEF_H__
#define __EXTDEF_H__

/* 最大的后缀名长度 */
#define MAX_EXT_SIZE            8

/* music related parser */
#define PARSER_EXT_MP3          "MP3"
#define PARSER_EXT_AAC          "AAC"
#define PARSER_EXT_WMA          "WMA"
#define PARSER_EXT_WMALSL       "WMALSL"
#define PARSER_EXT_WMAPRO       "WMAPRO"
#define PARSER_EXT_RMA          "RMA"
#define PARSER_EXT_WAV          "WAV"
#define PARSER_EXT_OGG          "OGG"
#define PARSER_EXT_DTS          "DTS"
#define PARSER_EXT_AC3          "AC3"
#define PARSER_EXT_APE          "APE"
#define PARSER_EXT_FLAC         "FLAC"
#define PARSER_EXT_MPC          "MPC"
#define PARSER_EXT_AIFF         "AIFF"
#define PARSER_EXT_AA           "AA"
#define PARSER_EXT_AAX          "AAX"
#define PARSER_EXT_AMR          "AMR"
#define PARSER_EXT_SAMPLE       "SAMPLE"
#define PARSER_EXT_ALAC         "ALAC"

/* music related decoder */
#define DEC_EXT_MP3             "MP3"
#define DEC_EXT_AAC             "AAC"
#define DEC_EXT_WMASTD          "WMASTD"
#define DEC_EXT_WMALSL          "WMALSL"
#define DEC_EXT_WMAPRO          "WMAPRO"
#define DEC_EXT_COOK            "COOK"
#define DEC_EXT_PCM             "PCM"
#define DEC_EXT_OGG             "OGG"
#define DEC_EXT_DTS             "DTS"
#define DEC_EXT_AC3             "AC3"
#define DEC_EXT_APE             "APE"
#define DEC_EXT_FLAC            "FLAC"
#define DEC_EXT_ACELP           "ACELP"
#define DEC_EXT_MPC             "MPC"
#define DEC_EXT_AIFF            "AIFF"
#define DEC_EXT_AMR             "AMR"
#define DEC_EXT_SAMPLE          "SAMPLE"
#define DEC_EXT_ALAC            "ALAC"
#define DEC_EXT_AWB             "AWB"

/* music related encoder */
#define ENC_EXT_MP3             "MP3"
#define ENC_EXT_WAV             "WAV"
#define ENC_EXT_SAMPLE          "SAMPLE"

#endif // __EXTDEF_H__
